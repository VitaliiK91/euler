﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler2
{
    class Program
    {
        static public int Fibonachi(int number)
        {
            if(number == 1 || number == 2)
            {
                return number;
            }
            else
            {
                return Fibonachi(number - 1) + Fibonachi(number - 2);
            }
        }

        static void Main(string[] args)
        {
            int sum = 0;

            Console.WriteLine("Inter limit:");

            var limit = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; ;i++)
            {
                int value = Fibonachi(i);

                if(value % 2 == 0)
                {
                    
                    if (value > limit)
                        break;
                    sum += value;
                }
            }

            Console.WriteLine("Sum of even values is:");
            Console.WriteLine(sum);
        }
    }
}
