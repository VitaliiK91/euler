﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler3
{
    class Program
    {
        static public bool IsPrime(int value)
        {
            for (int i = 2; i < Math.Sqrt(value) ; i++)
            {
                if (value % i == 0)
                    return false;
            }

            return true;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number:");

            var number = Convert.ToInt64(Console.ReadLine());
            int asnwer = 0;

            Console.WriteLine("Looking for the largest prime factor of the number {0} ...",number);

            for (int i = 2; i < Math.Sqrt(number); i++)
            {
                if(number % i == 0 && IsPrime(i))
                {
                    asnwer = i;
                }
            }

            Console.WriteLine("The largest prime factor of the number {0} is: {1}", number, asnwer);
        }
    }
}
