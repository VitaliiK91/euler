﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Euler28
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int res = 1;
            int count = 0;
            int step = 2;
            int currStep = 0;
            for (int i = 2; i <= 1002001; i++)
            {
                currStep++;
                if(currStep == step)
                {
                    res += i;
                    count++;
                    currStep = 0;
                }
                if(count==4)
                {
                    step += 2;
                    count = 0;
                }
            }
            sw.Stop();
            Console.WriteLine("result is {0}, it took {1} ms to calculate it", res, sw.ElapsedMilliseconds);
        }
    }
}
