﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Numerics;

namespace Euler54
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = EulerMath.readInputHands("poker.txt");
            int count = 0;
            for (int f = 0; f < i.Length; f+=2)
            {
                var one = i[f].Combination();
                var two = i[f+1].Combination();
                if (one.Points > two.Points)
                    count++;
                else if (one.Points == two.Points)
                    if (one.Value > two.Value)
                        count++;
                    else if (one.Value == two.Value)
                        if (one.BiggetCard > two.BiggetCard)
                            count++;
            }
            Console.WriteLine(count);
        }
    }
}
