﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Numerics;

namespace Euler52
{
    class Program
    {
        static void Main(string[] args)
        {
            for (BigInteger i = 125874; ; i++)
            {
                var x2 = ((i * 2).IntToArray());
                var x3 = (i * 3).IntToArray();
                var x4 = (i * 4).IntToArray();
                var x5 = (i * 5).IntToArray();
                var x6 = (i * 6).IntToArray();
                Array.Sort(x2);
                Array.Sort(x3);
                Array.Sort(x4);
                Array.Sort(x5);
                Array.Sort(x6);
                var x = i.IntToArray();
                Array.Sort(x);
                if (x.SequenceEqual(x2) && x.SequenceEqual(x3) && x.SequenceEqual(x4) && x.SequenceEqual(x5) && x.SequenceEqual(x6))
                {
                    Console.WriteLine(i);
                    return;
                }
            }
        }
    }
}
