﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Numerics;


namespace Euler58
{
    class Program
    {
        static void Main(string[] args)
        {
            int res = 1;
            int count = 0;
            int step = 2;
            int currStep = 0;
            double prime = 0;
            double all = 1;
            for (int i = 2; ; i++)
            {
                currStep++;
                if (currStep == step)
                {
                    if (EulerMath.IsPrime(i))
                        prime++;
                    all++;
                    // res += i;
                    count++;
                    currStep = 0;
                }
                if (count == 4)
                {
                    step += 2;
                    count = 0;
                    if (prime / all < 0.1)
                    {
                        Console.WriteLine((all - 1) / 2 + 1);
                        return;
                    }
                }
            }
        }
    }
}
