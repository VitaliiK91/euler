﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Diagnostics;

namespace Euler24
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string numbers = "0123456789";
            var j = Array.ConvertAll(numbers.ToCharArray(), c => (int)Char.GetNumericValue(c));
            //var lim = EulerMath.Factorial(numbers);
           
            for (int i = 0; i < 999999; i++)
            {
                j = EulerMath.NextPerm(j, j.Length);
            }
            sw.Stop();
            foreach (var item in j)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine(sw.Elapsed);
        }
    }
}
