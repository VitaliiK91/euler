﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler32
{
    class Program
    {
        static BigInteger ArrayToInt(int[] array)
        {
            int res = 0;
            for (int i = 0; i < array.Length; i++)
            {
                res += array[i] *(int) Math.Pow(10.0, (double)(array.Length - i));
            }
            return res;
        }
        public static int[] digitArr( int n)
        {
            if (n == 0) return new int[1] { 0 };

            var digits = new List<int>();

            for (; n != 0; n /= 10)
                digits.Add(n % 10);

            var arr = digits.ToArray();
            Array.Reverse(arr);
            return arr;
        }
        public static long ConcatInt(long a, long b, long c)
        {
            string str = a.ToString() + b.ToString() + c.ToString();
            return Convert.ToInt64(str);

        }
        static void Main(string[] args)
        {
            BigInteger res = 0;
            for (long i = 999; i < 100000; i++)
            {
                for (long j = 2; j <= Math.Sqrt(i); j++)
                {
                    var next = false;
                    if (i % j == 0)
                    {
                        var iden = ConcatInt(i, j, i / j);
                        if (iden.ToString().Length == 9 && !iden.ToString().Contains("0"))
                        {
                            if (EulerMath.IsPanDigital(iden))
                            {
                                Console.WriteLine("{0}*{1}={2}", j, i / j, i);
                                res += i;
                                next = true;
                            }
                        }
                    }
                    if (next)
                        break;
                }
            }
            Console.WriteLine(res);
        }
    }
}
