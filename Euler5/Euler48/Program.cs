﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Euler48
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger res = 0;
            for (int i = 1; i <= 1000; i++)
            {
                res += BigInteger.Pow(i, i);
            }
            Console.WriteLine(res);
        }
    }
}
