﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
namespace Euler22
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = System.IO.File.ReadAllText(@"names.txt").Replace("\"", "");
            string aplphabet = "_ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string[] namesArray = text.Split(',');
            var OrderedNamesArray = namesArray.OrderBy(i => i).ToArray();
            int res = 0;
            int tempRes = 0;
            for (int i = 0; i < OrderedNamesArray.Count(); i++)
            {
                var name = OrderedNamesArray[i].ToArray();
                foreach (var item in name)
                {
                    tempRes += aplphabet.IndexOf(item);
                }
                res += tempRes * (i + 1);
                tempRes = 0;
            }
            Console.WriteLine(res);
        }
    }
}
