﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler19
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime start = new DateTime(1901,1,1);
            DateTime end = new DateTime(2000, 12, 31);
            int count = 0;
            for (var i = start; i < end; i=i.AddDays(1.0))
            {
                if(i.Day == 1 && i.DayOfWeek == DayOfWeek.Sunday)
                {
                    count++;
                }
            }
            Console.WriteLine(count);
        }
    }
}
