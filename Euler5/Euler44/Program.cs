﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler44
{
    class Program
    {
        static void Main(string[] args)
        {
            var PentagonsQ = EulerMath.GetPentagonNumbers(3000);
            var Pentagons = PentagonsQ.ToArray();
            int res=1000000;
            for (int i = 1; i < Pentagons.Length; i++)
            {
                for (int k = i+1; k < Pentagons.Length; k++)
                {
                    var sum = Pentagons[i] + Pentagons[k];
                    var diff = Pentagons[k] - Pentagons[i];
                   // Console.WriteLine("{0}+{1}={2}",Pentagons[i],Pentagons[k],sum);
                   // Console.WriteLine("{0}-{1}={2}", Pentagons[i], Pentagons[k], diff);
                    if (PentagonsQ.Contains(sum) && PentagonsQ.Contains(diff))
                    {
                        res = diff;
                        Console.WriteLine(res);
                        return;
                    }
                }
            }
            //Console.WriteLine(res);
        }
    }
}
