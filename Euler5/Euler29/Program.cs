﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Diagnostics;
namespace Euler29
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Queue<BigInteger> Terms = new Queue<BigInteger>();
            for (int a = 2; a <= 100; a++)
            {
                for (int b = 2; b <= 100; b++)
                {
                    Terms.Enqueue((BigInteger)Math.Pow((double)a, (double)b));
                }
            }
            var i = Terms.Distinct().ToArray();
            sw.Stop();
            Console.WriteLine(i.Length);
            Console.WriteLine(sw.ElapsedMilliseconds);
        }
    }
}
