﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
namespace Euler21
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum=0;
            for (int i = 1; i < 10000; i++)
            {
                for (int j = 1; j < 10000; j++)
                {
                    if (EulerMath.IsAmicable(i, j))
                        sum += i;
                }
            }
            Console.WriteLine(sum);
        }
    }
}
