﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler26
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfDigits = 0;
            int res = 0;
            for (int i = 1; i < 1000; i++)
            {
                var tmp = EulerMath.DigitsInReccuringCycle(1,i);
                if (tmp > numberOfDigits)
                {
                    numberOfDigits = tmp;
                    res = i;
                }
            }
            Console.WriteLine(res);
        }

    }
}
