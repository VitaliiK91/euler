﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler38
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger res = 0;
            for (long i = 1; i < 1000000; i++)
            {
                bool next = true;
                string number = "";
                int n = 1;
                while(next)
                {
                   
                    number += n * i;
                    n++;
                    if (number.Length >= 9)
                        next = false;
                }
                var tmpRes = BigInteger.Parse(number);
                if (EulerMath.IsPanDigital(tmpRes) && tmpRes > res)
                {
                    Console.WriteLine(tmpRes);
                    res = tmpRes;
                }

            }
            Console.WriteLine(res);
        }
    }
}
