﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Numerics;
using System.Diagnostics;
namespace Euler31
{
    class Program
    {
        static public int F(int[] a, int i, int n)
        {
            if (n < 0)
                return 0;
            else if (n == 0)
                return 1;
            return i < 0 ? 0 : F(a, i - 1, n) + F(a, i, n - a[i]);
        }
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int[] a = new int[] { 1, 2, 5, 10,50, 20, 100, 200 };
            sw.Stop();
            Console.WriteLine(F(a, (a.Length - 1), 200));
            Console.WriteLine(sw.ElapsedTicks);
        }
    }
}
