﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler47
{
    class Program
    {
        static void Main(string[] args)
        {
            int inRow = 0;
            for (int i = 0; ; i++)
            {
                int count = 0;
                for (int j = 2; j * j <= i; j++)
                {
                    if (i % j == 0)
                    {
                        if (EulerMath.IsPrime(j))
                        {
                            count++;
                            
                        }
                        if (EulerMath.IsPrime(i / j))
                        {
                            count++;
                            
                        }

                    }
                }
                if (count == 4)
                    inRow++;

                else
                    inRow = 0;
                if (inRow == 4)
                {
                    Console.WriteLine(i-3);
                    return;
                }
            }
            
        }
    }
}
