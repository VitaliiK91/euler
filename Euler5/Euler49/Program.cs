﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler49
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 2593; i < 9876; i++)
            {
                if (i != 1487 && i != 4817 && EulerMath.IsPrime(i))
                {
                    bool next = true;
                    int j = i + 3330;
                    if (EulerMath.IsPrime(j))
                    {
                        foreach (var item in j.ToString())
                        {
                            if (!i.ToString().Contains(item))
                            {
                                next = false;
                                break;
                            }
                        }
                        if (next)
                        {
                            var f = j + 3330;
                            if (EulerMath.IsPrime(f))
                            {
                                foreach (var item in j.ToString())
                                {
                                    if (!j.ToString().Contains(item))
                                    {
                                        next = false;
                                        break;
                                    }
                                }
                                if (next)
                                {
                                    Console.WriteLine("{0}{1}{2}", i, j, f);

                                }
                            }
                            
                        }
                    }
                }
                    
            }
        }
    }
}
