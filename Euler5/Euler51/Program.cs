﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler51
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 100000; ; i++)
            {
                Console.WriteLine(i);
                var number = i.ToString();
                
                if (EulerMath.IsPrime(i))
                {
                    for (int j = 0; j < number.Length - 2; j++)
                    {
                        int count = 0;
                        for (int k = 0; k < 10; k++)
                        {
                            var NewNum = number.ToCharArray();
                            var intNewNum = Array.ConvertAll(NewNum, c => (int)Char.GetNumericValue(c));
                            if(k==0 && intNewNum[j]==intNewNum[j+1] && intNewNum[j]==intNewNum[j+2])
                                count++;
                            intNewNum[j] = k;
                            intNewNum[j + 1] = k;
                            intNewNum[j + 2] = k;
                            var num = EulerMath.ArrayToInt(intNewNum);
                            if (EulerMath.IsPrime(num) && num.ToString().Length == i.ToString().Length && num!=i)
                            {
                               // Console.WriteLine(num);
                                count++;
                            }
                            if (count == 8)
                            {
                                Console.WriteLine(i);
                                return;
                            }
                        }
                    }
                   
                }
            }
        }
    }
}
