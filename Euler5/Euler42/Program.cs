﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler42
{
    class Program
    {
        static void Main(string[] args)
        {
            var Triangles = EulerMath.GetTriangleNUmbersArray(500);
            var words = EulerMath.readInput("source.txt");
            var res = words.Where(i => Triangles.Contains(i.Value)).Count();
            Console.WriteLine(res);
              
        }
    }
}
