﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Diagnostics;
namespace Euler39
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int a = 1, b = 2;
            Dictionary<int,int> Triangles = new Dictionary<int,int>();

            for ( a = 1; a <400; a++)
            {
                for ( b = a+1; b < 400; b++)
                {
                   var c = Math.Sqrt((a * a + b * b));
                    if(c % (int)c == 0)
                    {
                        var p =a+b+(int)c;
                        if (Triangles.ContainsKey(p))
                            Triangles[p]++;
                        else
                            Triangles.Add(p, 1);
                    }
                }
            }
            var max = Triangles.Aggregate((l, r) => l.Value > r.Value ? l : r);
            sw.Stop();
            Console.WriteLine("{0} is the perimeter with {1} possible solutions",max.Key,max.Value);
            Console.WriteLine(sw.ElapsedMilliseconds);
        }
    }
}
