﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Diagnostics;

namespace Euler23
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int res = 0;
            for (int i = 0; i < 28123; i++)
            {
                if (!EulerMath.CanBeWrittenAsSumOfTwoAbundantNumbers(i))
                    res += i;
            }
            sw.Stop();
            Console.WriteLine(res);
            Console.WriteLine(sw.Elapsed);
        }
    }
}
