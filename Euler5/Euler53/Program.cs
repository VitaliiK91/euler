﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler53
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 0;
            for (int n = 1; n <= 100; n++)
            {
                for (int r = 1; r <= n; r++)
                {
                    if (EulerMath.Combination(n, r) > 1000000)
                        count++;
                }
            }
            Console.WriteLine(count);
        }
    }
}
