﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
namespace Euler14
{
    class Program
    {
        static void Main(string[] args)
        {

            int counter = 0;
            long res = 0;
            for (long j = 2; j < 1000000; j++)
            {
                long i = j;
                int counterTmp = 1;
                while (i != 1)
                {
                    i = i.NextCollatz();
                    counterTmp++;
                }
                if (counterTmp > counter)
                {
                    counter = counterTmp;
                    res = j;
                }
            }
            Console.WriteLine(res);
        }
    }
}
