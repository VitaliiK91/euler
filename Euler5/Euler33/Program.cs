﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler33
{
    class Program
    {
        public static bool IsCuriousFraction(int numerator, int denominator)
        {
            double fraction = (double)numerator / (double)denominator;
            var Numerator = numerator.ToString().ToCharArray();
            var Denominator = denominator.ToString().ToCharArray();
            if(Numerator[0] == Denominator[0] && Numerator[1]!=Denominator[1] && Numerator[1]!='0')
            {
                double SimpleFraction = Char.GetNumericValue(Numerator[1]) / Char.GetNumericValue(Denominator[1]);
                if (fraction == SimpleFraction)
                    return true;
            }
            else if (Numerator[1] == Denominator[1] && Numerator[0] != Denominator[0] && Numerator[1].ToString() != "0")
            {
                double SimpleFraction = Char.GetNumericValue(Numerator[0]) / Char.GetNumericValue(Denominator[0]);
                if (fraction == SimpleFraction)
                    return true;
            }
            else if(Numerator[1]==Denominator[0])
            {
                double SimpleFraction = Char.GetNumericValue(Numerator[0]) / Char.GetNumericValue(Denominator[1]);
                if (fraction == SimpleFraction)
                    return true;
            }
            else if(Numerator[0]==Denominator[1])
            {
                double SimpleFraction = Char.GetNumericValue(Numerator[1]) / Char.GetNumericValue(Denominator[0]);
                if (fraction == SimpleFraction)
                    return true;
            }
            return false;
        }
        static void Main(string[] args)
        {
            var den = 1;
            var nom = 1;
            for (int i = 10; i < 100; i++)
            {
                for (int j = i + 1; j < 100; j++)
                {
                    if (IsCuriousFraction(i, j))
                    {
                        Console.WriteLine("{0} / {1}", i, j);
                        nom *= i;
                        den *= j;
                    }
                }
            }
            Console.WriteLine("{0} / {1} = {2}", nom, den, (double)nom / den);
        }
    }
}
