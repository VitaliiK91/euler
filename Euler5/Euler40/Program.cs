﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler40
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 0;
            int prefix = 0;
            Queue<int> Res = new Queue<int>();

            for (int i = 0; Res.Count <= 1000000; i++)
            {
                if (count == 10)
                {
                    count = 0;
                    prefix++;
                }
                if (prefix > 0)
                {
                    for (int f = 0; f < prefix.ToString().Length; f++)
                    {
                        Res.Enqueue(Convert.ToInt32(Char.GetNumericValue(prefix.ToString()[f])));
                    }
                    Res.Enqueue(count);
                }
                else
                {
                    Res.Enqueue(count);
                }
                count++;
            }

            var res = Res.ElementAt(1) * Res.ElementAt(10) * Res.ElementAt(100) * Res.ElementAt(1000) * Res.ElementAt(10000) * Res.ElementAt(100000) * Res.ElementAt(1000000);
            Console.WriteLine(res);
        }
    }
}
