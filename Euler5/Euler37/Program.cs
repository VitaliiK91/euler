﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler37
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 0;
            int res = 0;
            for (int i = 10; count < 11; i++)
            {
                
                if (EulerMath.IsTruncatablePrime(i))
                {
                    Console.WriteLine(i);
                    count++;
                    res+=i;
                }
            }
            Console.WriteLine(res);
        }
    }
}
