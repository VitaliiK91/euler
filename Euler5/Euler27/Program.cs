﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler27
{
    class Program
    {
        static void Main(string[] args)
        {
            int primes = 0;
            int res = 0;
            //for (int i = 0; i < 1011; i++)
            //{
            //    var f = i * i - 999 * i + 61;
            //    if (!EulerMath.IsPrime(f))
            //        Console.WriteLine("bad");
            //    else
            //        Console.WriteLine(f);
                
            //}
            for (int i = -999; i < 1000; i++)
            {
                // Console.WriteLine(i);
                for (int j = -999; j < 1000; j++)
                {
                    var ex = 0;
                    for (int n = 0; ; n++)
                    {
                        var f = n * n + i * n + j;
                        if (f < 0)
                            ex++;
                        if (!EulerMath.IsPrime(f))
                        {
                            if ((n-ex) > primes)
                            {
                                primes = (n - ex);
                                res = i * j;
                                Console.WriteLine("a is {0}, b is {1}, product is {2}, {3} primes", i, j, res, primes);
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
}
