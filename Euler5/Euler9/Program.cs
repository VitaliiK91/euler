﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyMath;
namespace Euler9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Processing ..");
            for (int i = 1; i < 1000; i++)
            {
                for (int j = i + 1; j < 1000; j++)
                {
                    var a = j * j - i * i;
                    var b = 2 * j * i;
                    var c = j * j + i * i;
                    var sum = a + b + c;
                    if (sum == 1000)
                    {
                        Console.WriteLine(a * b * c);
                        return;
                    }
                }
            }
        }
    }
}
