﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler16
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = Math.Pow((double)2.0, (double)1000.0);
            var str = DoubleConverter.ToExactString(i);
            var ch = str.ToCharArray();
            //string f = Math.Pow(2, 1000);           
            var ints = Array.ConvertAll(ch, c => (int)Char.GetNumericValue(c));
            long sum = 0;
            foreach (var item in ints)
            {
                if (item > 0)
                    sum += item;
            }
            Console.WriteLine(sum);
        }
    }
}
