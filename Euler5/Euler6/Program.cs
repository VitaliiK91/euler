﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler6
{
    class Program
    {
        public static int SumOfSqaresOfFirstXNumbers(int x)
        {
            int result = 0;
            for (int i = 0; i <= x; i++)
            {
                result += i * i;
            }
            return result;
        }
        public static int SquareOfSumeOfFirstXNumbers(int x)
        {
            int result = 0;
            for (int i = 0; i <= x; i++)
            {
                result += i;
            }
            return result * result;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(SquareOfSumeOfFirstXNumbers(100) - SumOfSqaresOfFirstXNumbers(100));
        }
    }
}
