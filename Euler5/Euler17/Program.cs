﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler17
{
    class Program
    {
        static void Main(string[] args)
        {
            int res=0;
            for (int i = 1; i <= 1000; i++)
            {
                var str = EulerMath.NumberToWords(i);
                res += str.Replace(" ","").Replace("-","").Length;
            }
            Console.WriteLine(res);
        }
    }
}
