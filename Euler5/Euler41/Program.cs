﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler41
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] Digits = new int[] { 1, 2, 3, 4, 5, 6, 7 };
            BigInteger res = 0;
            while(true)
            {
                var next = EulerMath.NextPerm(Digits, Digits.Length);
                var Next = EulerMath.ArrayToInt(next);
                if (Next < 1)
                    break;
                if(Next > res)
                {
                    if(EulerMath.IsPrime(Next))
                    {
                        res = Next;
                    }
                }
            }
            Console.WriteLine(res);
        }
    }
}
