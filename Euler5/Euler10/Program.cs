﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyMath;

namespace Euler10
{
    class Program
    {
        static void Main(string[] args)
        {
            long sum = 0;
            for (int i = 2; i < 2000000; i++)
            {
                if (EulerMath.IsPrime(i))
                    sum += i;
            }
            Console.WriteLine(sum);
        }
    }
}
