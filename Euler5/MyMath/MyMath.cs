﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.IO;

namespace MyMath
{
    public static class EulerMath
    {
        static public BigInteger Fibonachi(int number)
        {
            if (number == 1 || number == 2)
            {
                return number;
            }
            else
            {
                return Fibonachi(number - 1) + Fibonachi(number - 2);
            }
        }
        static public BigInteger FibonachiItter(int n)
        {
            if (n <= 1)
            {
                return n;
            }
            BigInteger fibo = 1;
            BigInteger fiboPrev = 1;
            for (int i = 2; i < n; ++i)
            {
                BigInteger temp = fibo;
                fibo += fiboPrev;
                fiboPrev = temp;
            }
            return fibo;
        }
        static public bool IsPrime(int value)
        {
            if (value == 0 || value == 1)
                return false;
            for (int i = 2; i <= Math.Sqrt(value); i++)
            {
                if (value % i == 0)
                    return false;
            }

            return true;
        }
        static public bool IsPrime(BigInteger value)
        {
            if (value == 0 || value == 1)
                return false;
            for (int i = 2; i <= Math.Sqrt((double)value); i++)
            {
                if (value % i == 0)
                    return false;
            }

            return true;
        }
        static public bool IsPalindromic(int value)
        {
            int reverse = 0;
            int original = value;
            while (value > 0)
            {
                reverse = reverse * 10 + value % 10;
                value /= 10;
            }
            if (reverse == original)
                return true;
            else
                return false;
        }
        static public bool IsPalindromic(BigInteger value)
        {
            BigInteger reverse = 0;
            BigInteger original = value;
            while (value > 0)
            {
                reverse = reverse * 10 + value % 10;
                value /= 10;
            }
            if (reverse == original)
                return true;
            else
                return false;
        }
        public static bool IsDevidedByEachNumberFrom1(int to, int value)
        {
            for (int i = 1; i < to; i++)
            {
                if (value % i != 0)
                    return false;
            }
            return true;
        }
        public static int SumOfSqaresOfFirstXNumbers(int x)
        {
            int result = 0;
            for (int i = 0; i <= x; i++)
            {
                result += i * i;
            }
            return result;
        }
        public static int SquareOfSumeOfFirstXNumbers(int x)
        {
            int result = 0;
            for (int i = 0; i <= x; i++)
            {
                result += i;
            }
            return result * result;
        }
        public static bool IsPythagoreanTriplet(int a, int b, int c)
        {
            if (a * a + b * b == c * c)
                return true;
            else
                return false;
        }
        public static int TriangleNumer(int value)
        {
            var res = 0;
            for (int i = 1; i <= value; i++)
            {
                res += i;
            }
            return res;
        }

        public static long NextCollatz(this long value)
        {
            if (value % 2 == 0)
                return value /= 2;
            else
                return 3 * value + 1;
        }
        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }
        public static BigInteger Factorial(int value)
        {
            if (value > 2)
                return value;
            else
                return value * Factorial(value - 1);
        }

        public static BigInteger FactorialNonRecursive(int n)
        {
            BigInteger result = 1;
            for (int i = 2; i <= n; ++i)
            {
                result *= i;
            }
            return result;
        }

        public static int SumOfDivisors(int value)
        {
            int res = 0;
            int lim = (int)Math.Sqrt(value);
            for (int i = 1; i <= lim; i++)
            {
                if (value % i == 0)
                {
                    if (i == value / i || i == 1)
                        res += i;
                    else
                        res += i + value / i;
                }
            }
            return res;
        }
        public static bool IsAmicable(int x, int y)
        {
            if (SumOfDivisors(x) == y && SumOfDivisors(y) == x && x != y)
                return true;
            else
                return false;
        }
        public static bool IsAbundant(int value)
        {
            return (SumOfDivisors(value) > value);
        }
        public static bool CanBeWrittenAsSumOfTwoAbundantNumbers(int value)
        {
            for (int i = 0, j = value; i <= value / 2; i++, j--)
            {
                if (IsAbundant(i) && IsAbundant(j))
                    return true;
            }
            return false;
        }
        public static void swap(ref int a, ref int b)
        {
            int temp = a;
            a = b;
            b = temp;
        }
        public static int[] NextPerm(int[] v, int n)
        {
            /* Find the largest i */
            int i = n - 2;
            while ((i >= 0) && (v[i] > v[i + 1]))
                --i;

            /* If i is smaller than 0, then there are no more permutations. */
            if (i < 0)
                return new int[] { 0, 0 };

            /* Find the largest element after vi but not larger than vi */
            int k = n - 1;
            while (v[i] > v[k])
                --k;
            swap(ref v[i], ref v[k]);

            /* Swap the last n - i elements. */
            int j;
            k = 0;
            for (j = i + 1; j < (n + i) / 2 + 1; ++j, ++k)
                swap(ref v[j], ref v[n - k - 1]);

            return v;
        }

        public static int DigitsInReccuringCycle(int numerator, int denominator)
        {
            int l = 0;
            double c = 0, e = 0;
            double r = Convert.ToDouble(numerator);
            e = r;
            double n = Convert.ToDouble(denominator);
            while (l != n + 1)
            {
                r = (10 * r) % n;
                l++;
            }
            c = r;
            r = (10 * r) % n;
            int k = 0;
            if (c != 0)
            {
                while (r != c)
                {
                    r = (10 * r) % n;
                    k++;
                }
                k++;
            }
            return k;
        }

        static public bool IsPanDigital(BigInteger number)
        {
            var Ch = number.ToString().ToCharArray().ToList();
            Queue<char> ChNew = new Queue<char>();
            for (int i = 0; i < Ch.Count; i++)
            {
                if (Ch[i] == '0')
                    return false;
                if (!ChNew.Contains(Ch[i]))
                    ChNew.Enqueue(Ch[i]);
                else
                    return false;
            }
            return true;
        }
        static public bool IsPanDigital(string number)
        {
            var Ch = number.ToCharArray().ToList();
            Queue<char> ChNew = new Queue<char>();
            for (int i = 0; i < Ch.Count; i++)
            {
                if (Ch[i] == '0')
                    return false;
                if (!ChNew.Contains(Ch[i]))
                    ChNew.Enqueue(Ch[i]);
                else
                    return false;
            }
            return true;
        }
        public static int ArrayToInt(int[] array)
        {
            int res = 0;
            for (int i = 0; i < array.Length; i++)
            {
                res += array[i] * (int)Math.Pow(10.0, (double)(array.Length - 1 - i));
            }
            return res;
        }
        public static BigInteger ArrayToBigInt(int[] array)
        {
            BigInteger res = 0;
            for (int i = 0; i < array.Length; i++)
            {
                res += array[i] * (BigInteger)Math.Pow(10.0, (double)(array.Length - 1 - i));
            }
            return res;
        }
        public static int NextRotation(int number, int original)
        {
            var Number = number.ToString().ToCharArray();
            int[] Result = new int[Number.Length];
            for (int i = 0; i < Number.Length; i++)
            {
                if (i != (Number.Length - 1))
                {
                    Result[i + 1] = (int)Char.GetNumericValue(Number[i]);
                }
                else
                {
                    Result[0] = (int)Char.GetNumericValue(Number[i]);
                }
            }
            string resStr = "";
            foreach (var item in Result)
            {
                resStr += item;
            }
            var res = Convert.ToInt32(resStr);
            if (res != original)
                return res;
            else
                return 0;
        }
        public static bool IsTruncatablePrime(int number)
        {
            if (!IsPrime(number))
                return false;
            int left = number;
            var pow = 1;
            while (left != 0)
            {
                left = left / 10;
                var right = Convert.ToInt32(number - left * Math.Pow(10.0, pow));
                if (left == 0 && pow == number.ToString().Length)
                    break;
                pow++;
                if (!IsPrime(left) || !IsPrime(right))
                    return false;
            }
            return true;
        }
        public static Queue<int> GetTriangleNUmbersArray(int limit)
        {
            Queue<int> Res = new Queue<int>();
            for (int i = 1; i < limit; i++)
            {
                Res.Enqueue((int)(i / 2.0 * (i + 1)));
            }
            return Res;
        }
        public static Queue<double> GetTriangleNUmbersArray(long limit)
        {
            Queue<double> Res = new Queue<double>();
            for (int i = 1; i < limit; i++)
            {
                Res.Enqueue((int)(i / 2.0 * (i + 1)));
            }
            return Res;
        }
        public static Dictionary<int, int> readInput(string filename)
        {
            string line;
            string[] linePieces;
            int lines = 0;
            Dictionary<int, int> Res = new Dictionary<int, int>();

            StreamReader r = new StreamReader(filename);
            while ((line = r.ReadLine()) != null)
            {
                lines++;
            }

            //int[,] inputTriangle = new int[lines, lines];
            r.BaseStream.Seek(0, SeekOrigin.Begin);

            int j = 0;
            while ((line = r.ReadLine()) != null)
            {

                linePieces = line.Replace('"', ' ').Trim().Split(',');
                for (int i = 0; i < linePieces.Length; i++)
                {
                    var res = 0;
                    foreach (var item in linePieces[i].ToCharArray())
                    {
                        res += (int)item % 32;
                    }
                    Res.Add(i, res);
                }

            }
            r.Close();
            return Res;
        }

        public static int[] IntToArray(this BigInteger number)
        {
            string str = number.ToString();
            int[] Res = new int[str.Length];
            for (int i = 0; i < str.Length; i++)
            {
                Res[i] = (int)Char.GetNumericValue(str[i]);
            }
            return Res;
        }

        public static bool IsSubStringDivisible(int[] Number)
        {
            var d1 = Convert.ToInt32(String.Format("{0}{1}{2}", Number[1], Number[2], Number[3]));
            var d2 = Convert.ToInt32(String.Format("{0}{1}{2}", Number[2], Number[3], Number[4]));
            var d3 = Convert.ToInt32(String.Format("{0}{1}{2}", Number[3], Number[4], Number[5]));
            var d4 = Convert.ToInt32(String.Format("{0}{1}{2}", Number[4], Number[5], Number[6]));
            var d5 = Convert.ToInt32(String.Format("{0}{1}{2}", Number[5], Number[6], Number[7]));
            var d6 = Convert.ToInt32(String.Format("{0}{1}{2}", Number[6], Number[7], Number[8]));
            var d7 = Convert.ToInt32(String.Format("{0}{1}{2}", Number[7], Number[8], Number[9]));
            return ((d1 % 2 == 0) && (d2 % 3 == 0) && (d3 % 5 == 0) && (d4 % 7 == 0) && (d5 % 11 == 0) && (d6 % 13 == 0) && (d7 % 17 == 0));
        }

        public static Queue<int> GetPentagonNumbers(int to)
        {
            Queue<int> Res = new Queue<int>();
            for (int i = 1; i < to; i++)
            {
                Res.Enqueue(i * (3 * i - 1) / 2);
            }
            return Res;
        }
        public static Queue<int> GetHexagonalNumbers(int to)
        {
            Queue<int> Res = new Queue<int>();
            for (int i = 1; i < to; i++)
            {
                Res.Enqueue(i * (2 * i - 1));
            }
            return Res;
        }
        public static Queue<double> GetPentagonNumbers(long to)
        {
            Queue<double> Res = new Queue<double>();
            for (int i = 1; i < to; i++)
            {
                Res.Enqueue(i * (3 * i - 1) / 2);
            }
            return Res;
        }
        public static Queue<double> GetHexagonalNumbers(long to)
        {
            Queue<double> Res = new Queue<double>();
            for (int i = 1; i < to; i++)
            {
                Res.Enqueue(i * (2 * i - 1));
            }
            return Res;
        }
        public static bool IsPentagonal(BigInteger number)
        {
            var res = (Math.Sqrt((double)(24 * number + 1)) + 1) / 6;

            if (res % 1 == 0)
                return true;
            return false;

        }
        public static bool IsGoldbach(int number)
        {
            for (int i = number - 2; i > 2; i--)
            {
                if (IsPrime(i))
                {
                    var mod = number - i;
                    var sq = Math.Sqrt(mod / 2.0);
                    if (sq % 1 == 0)
                        return true;
                }
            }
            return false;
        }

        public static BigInteger Combination(int n, int r)
        {
            return FactorialNonRecursive(n) / (FactorialNonRecursive(r) * FactorialNonRecursive(n - r));
        }
        public class Hand
        {
            public int ID { get; set; }
            public int Player { get; set; }
            public string[] Cards { get; set; }

            private int CardToValue(string card)
            {
                if (card.Contains("1"))
                    return 1;
                else if (card.Contains("2"))
                    return 2;
                else if (card.Contains("3"))
                    return 3;
                else if (card.Contains("4"))
                    return 4;
                else if (card.Contains("5"))
                    return 5;
                else if (card.Contains("6"))
                    return 6;
                else if (card.Contains("7"))
                    return 7;
                else if (card.Contains("8"))
                    return 8;
                else if (card.Contains("9"))
                    return 9;
                else if (card.Contains("T"))
                    return 10;
                else if (card.Contains("J"))
                    return 11;
                else if (card.Contains("Q"))
                    return 12;
                else if (card.Contains("K"))
                    return 13;
                else if (card.Contains("A"))
                    return 14;
                return 0;
            }
            private int GetCardsFace(string card)
            {
                if (card.Contains("S"))
                    return 1;
                else if (card.Contains("H"))
                    return 2;
                else if (card.Contains("D"))
                    return 3;
                else if (card.Contains("C"))
                    return 4;
                return 0;
            }
            public CombinationCard Combination()
            {
                CombinationCard combination = new CombinationCard();
                int biggest = 0;
                int[] values = new int[5];
                int[] faces = new int[5];
                Dictionary<int, int> valuesDict = new Dictionary<int, int>();
                Dictionary<int, int> facesDict = new Dictionary<int, int>();
                int i = 0;
                foreach (var card in this.Cards)
                {
                    values[i] = CardToValue(card);
                    faces[i] = GetCardsFace(card);

                    if (valuesDict.ContainsKey(values[i]))
                        valuesDict[values[i]]++;
                    else
                        valuesDict.Add(values[i], 1);

                    if (facesDict.ContainsKey(faces[i]))
                        facesDict[faces[i]]++;
                    else
                        facesDict.Add(faces[i], 1);

                    if (values[i] > biggest)
                        biggest = values[i];
                    i++;
                }
                StringBuilder comb = new StringBuilder("");
                bool twopairs = false;
                bool pair = false;
                bool three = false;
                bool four = false;
                foreach (var item in valuesDict)
                {
                    if (pair)
                        if (item.Value == 2)
                        {
                            twopairs = true;
                            combination.Points = 2;
                            combination.Value2 = item.Key;
                        }
                        else if (item.Value == 3)
                        {
                            combination.Name = "Full House";
                            combination.Points = 6;
                            combination.Value2 = item.Key;
                        }
                    if (item.Value == 2)
                    {
                        comb.Append(String.Format("pair of {0},", item.Key)); pair = true;
                        if (combination.Points < 2)
                        {
                            combination.Name = "pair"; combination.Points = 1; combination.Value = item.Key;
                        }
                    }
                    if (item.Value == 3)
                    {
                        comb.Append(String.Format("three of {0},", item.Key)); three = true;
                        if (combination.Points < 3)
                        {
                            combination.Name = "three"; combination.Points = 3; combination.Value = item.Key;
                        }
                    }
                    if (item.Value == 4)
                    {
                        comb.Append(String.Format("4 of {0},", item.Key)); four = true;
                        combination.Name = "four"; combination.Points = 7; combination.Value = item.Key;
                    }
                }
                bool flush = false;
                foreach (var item in facesDict)
                {
                    if (item.Value == 5)
                    {
                        comb.Append("Flush,");
                        flush = true;
                    }
                }
                Array.Sort(values);
                bool straight = true;
                bool royal = false;
                for (int f = 0; f < 4; f++)
                {

                    if (values[f] + 1 != values[f + 1])
                    {
                        straight = false;
                        break;
                    }
                    if (f == 4 && straight && values[f + 1] == 14)
                        royal = true;
                }
                if (pair)
                    if (three)
                    {
                        comb.Append("Full House, ");

                    }
                if (straight)
                    if (royal && flush)
                    {
                        comb.Append("Royal Flush,");
                        combination.Points = 9;
                        combination.Name = "Royal Flush";
                    }
                    else if (flush)
                    {
                        comb.Append("Straight Flush,");
                        combination.Points = 8;
                        combination.Name = "Straight Flush";
                    }
                    else
                    {
                        if (combination.Points < 5)
                        {
                            comb.Append("Flush");
                            combination.Points = 5;
                            combination.Name = "Fluish";
                        }
                    }


                if (twopairs)
                    comb.Append("2 pairs, ");

                comb.Append(String.Format("The biggest card is {0}", biggest));
                combination.BiggetCard = biggest;

                return combination;
            }
        }
        public class CombinationCard
        {
            public string Name { get; set; }
            public int Value { get; set; }
            public int? Value2 { get; set; }
            public int Points { get; set; }
            public int BiggetCard { get; set; }
        }
        public static Hand[] readInputHands(string filename)
        {
            string line;
            string[] linePieces;
            int lines = 0;
            Hand[] Hands = new Hand[2000];
            Dictionary<int, int> Res = new Dictionary<int, int>();

            StreamReader r = new StreamReader(filename);
            while ((line = r.ReadLine()) != null)
            {
                lines++;
            }

            //int[,] inputTriangle = new int[lines, lines];
            r.BaseStream.Seek(0, SeekOrigin.Begin);

            int j = 0;
            int id = 0;
            while ((line = r.ReadLine()) != null)
            {

                linePieces = line.Split(' ');
                Hands[j] = new Hand() { ID = id, Player = 1, Cards = linePieces.Take(5).ToArray() };
                j++;
                Hands[j] = new Hand() { ID = id, Player = 1, Cards = linePieces.Skip(5).ToArray() };
                j++;
                id++;
            }
            r.Close();
            return Hands;
        }
        public static BigInteger ReverseInt(BigInteger value)
        {
            BigInteger reverse = 0;
            BigInteger original = value;
            while (value > 0)
            {
                reverse = reverse * 10 + value % 10;
                value /= 10;
            }
            return reverse;
        }

        public static bool IsLychrel(BigInteger number)
        {
            BigInteger or = number;
            var rev = ReverseInt(number);
            for (int i = 0; i < 50; i++)
            {
                if (IsPalindromic(rev + or))
                    return true;
                else
                {
                    or = or + rev;
                    rev = ReverseInt(or);
                }
            }
            return false;
        }

      
    }
}
