﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler55
{
    class Program
    {
        static void Main(string[] args)
        {
            int count=0;
            Console.WriteLine();
            for (BigInteger i = 11; i < 10000; i++)
            {
                if (!EulerMath.IsLychrel(i))
                    count++;
            }
            Console.WriteLine(count);
        }
    }
}
