﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Numerics;
namespace Euler36
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger res = 0;
            for (long i = 1; i < 1000000; i++)
            {
                if(EulerMath.IsPalindromic(i))
                {
                    BigInteger j = Convert.ToUInt64(Convert.ToString(i, 2));
                    if (EulerMath.IsPalindromic(j))
                        res += i;
                }
            }
            Console.WriteLine(res);
        }
    }
}
