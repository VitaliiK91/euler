﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler15
{
    class Program
    {
        static void Main(string[] args)
        {
            int size = 20;
            long[,] grid = new long[size + 1, size + 1];
            for (int i = 0; i < size + 1; i++)
            {
                for (int j = 0; j < size + 1; j++)
                {
                    if (i == 0 || j == 0)
                        grid[i, j] = 1;
                }
            }
            for (int i = 1; i < size; i++)
            {
                for (int j = 1; j < size; j++)
                {
                    grid[i, j] = grid[i - 1, j] + grid[i, j - 1];
                }
            }
            Console.WriteLine(grid[size - 1, size - 1]);
        }
    }
}
