﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Numerics;
namespace Euler34
{
    class Program
    {
        public static bool IsCuriosNumber(int number)
        {
            var Number = number.ToString().ToCharArray();
            int res = 0;
            foreach (var item in Number)
            {
                res += (int) EulerMath.FactorialNonRecursive((int)Char.GetNumericValue(item));
            }
            return res == number;
        }
        static void Main(string[] args)
        {
            int res = 0;
            for (int i = 3; i < 100000; i++)
            {
                if (IsCuriosNumber(i))
                {
                    Console.WriteLine(i);
                    res += i;
                }
            }
            Console.WriteLine(res);
        }
    }
}
