﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler59
{
    class Program
    {
        public static string[] readCipher(string filename)
        {
            string line;
            string[] linePieces = new string[1201];
            int lines = 0;
            byte[] butes = new byte[1201];
            Dictionary<int, int> Res = new Dictionary<int, int>();

            StreamReader r = new StreamReader(filename);
            while ((line = r.ReadLine()) != null)
            {
                lines++;
            }

            //int[,] inputTriangle = new int[lines, lines];
            r.BaseStream.Seek(0, SeekOrigin.Begin);

            int j = 0;
            int id = 0;
            while ((line = r.ReadLine()) != null)
            {

                linePieces = line.Split(',');
               
            }
            r.Close();
            return linePieces;
        }
        static void Main(string[] args)
        {
            var Cipher = readCipher("cipher.txt");
            var byt = Cipher.Select(byte.Parse).ToArray();
            
            string[] strings = new string[20000];
            int b = 0;
            
            for (byte i = 97; i < 122; i++)
            {
                for (byte k = 97; k < 122; k++)
                {
                    for (byte m = 97; m < 122; m++)
                    {
                        string text;
                        int j = 0;
                        int intRes = 0;
                        byte[] res = new byte[1201];
                        for (int f = 2; f <1201; f += 3)
                        {
                            res[j] = (byte)(byt[f - 2] ^ i);
                            res[j + 1] = (byte)(byt[f - 1] ^ k);
                            res[j + 2] = (byte)(byt[f] ^ m);
                            intRes += res[j];
                            intRes += res[j + 1];
                            intRes += res[j + 2];
                            j += 3;
                        }
                        text = System.Text.Encoding.ASCII.GetString(res);
                        if (!text.ToLower().Contains("{") && !text.ToLower().Contains("}") &&
                            !text.ToLower().Contains("[")&&!text.ToLower().Contains("]") ) 
                        {
                            byte[] asciiBytes = Encoding.ASCII.GetBytes(text);
                            Console.WriteLine(text);
                            Console.WriteLine("{0}", intRes);
                        }
                        //strings[b] = System.Text.Encoding.ASCII.GetString(res);
                        //b++;
                        
                    }
                }
            }
            Console.WriteLine();
        }
    }
}
