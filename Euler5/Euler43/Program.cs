﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler43
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] Digits = new int[] { 1, 0, 2, 3, 4, 5, 6, 7, 8, 9 };
            BigInteger res = 0;
            int i = 0;
            while (true)
            {
                var next = EulerMath.NextPerm(Digits, Digits.Length);
                BigInteger nextInt = EulerMath.ArrayToBigInt(next);

                if (nextInt < 1)
                    break;

                if (EulerMath.IsSubStringDivisible(next))
                {
                    Console.WriteLine(nextInt);
                    res += nextInt;
                }
                i++;
            }
            Console.WriteLine(res);
        }
    }
}
