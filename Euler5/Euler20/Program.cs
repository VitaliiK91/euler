﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler20
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            
            BigInteger x = EulerMath.Factorial(100);
            var str = EulerMath.Factorial(100).ToString();
            var fact = Array.ConvertAll(str.ToCharArray(), c => (int)Char.GetNumericValue(c));
            foreach (var item in fact)
            {
                if (item > 0)
                    sum += item;
            }
            Console.WriteLine(sum);
        }
    }
}
