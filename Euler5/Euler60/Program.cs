﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler60
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 3; i<4; i++)
            {
                if (EulerMath.IsPrime(i))
                {
                    for (int j = 7; j<8; j++)
                    {
                        if (EulerMath.IsPrime(j))
                        {
                            for (int k = 109; k<500; k++)
                            {
                                if (EulerMath.IsPrime(k))
                                {
                                    for (int m = 673;m<2000 ; m++)
                                    {
                                        if (EulerMath.IsPrime(m))
                                        {
                                            for (int f = m+1; f<5000; f++)
                                            {

                                                if (EulerMath.IsPrime(f))
                                                {
                                                    //if (EulerMath.IsPrime(i + j + k + m))
                                                    //{
                                                    int var1 = Convert.ToInt32(i.ToString() + j.ToString());
                                                    int var2 = Convert.ToInt32(j.ToString() + i.ToString());
                                                    int var3 = Convert.ToInt32(i.ToString() + k.ToString());
                                                    int var4 = Convert.ToInt32(k.ToString() + i.ToString());
                                                    int var5 = Convert.ToInt32(i.ToString() + m.ToString());
                                                    int var6 = Convert.ToInt32(m.ToString() + i.ToString());
                                                    int var7 = Convert.ToInt32(j.ToString() + k.ToString());
                                                    int var8 = Convert.ToInt32(k.ToString() + j.ToString());
                                                    int var9 = Convert.ToInt32(j.ToString() + m.ToString());
                                                    int var10 = Convert.ToInt32(m.ToString() + j.ToString());
                                                    int var11 = Convert.ToInt32(k.ToString() + m.ToString());
                                                    int var12 = Convert.ToInt32(m.ToString() + k.ToString());
                                                    int var13 = Convert.ToInt32(i.ToString() + f.ToString());
                                                    int var14 = Convert.ToInt32(f.ToString() + i.ToString());
                                                    int var15 = Convert.ToInt32(j.ToString() + f.ToString());
                                                    int var16 = Convert.ToInt32(f.ToString() + j.ToString());
                                                    int var17 = Convert.ToInt32(k.ToString() + f.ToString());
                                                    int var18 = Convert.ToInt32(f.ToString() + k.ToString());
                                                    int var19 = Convert.ToInt32(m.ToString() + f.ToString());
                                                    int var20 = Convert.ToInt32(f.ToString() + m.ToString());
                                                    if (EulerMath.IsPrime(var1) && EulerMath.IsPrime(var2) &&
                                                        EulerMath.IsPrime(var3) && EulerMath.IsPrime(var4) &&
                                                        EulerMath.IsPrime(var5) && EulerMath.IsPrime(var6) &&
                                                        EulerMath.IsPrime(var7) && EulerMath.IsPrime(var8) &&
                                                        EulerMath.IsPrime(var9) && EulerMath.IsPrime(var10) &&
                                                        EulerMath.IsPrime(var11) && EulerMath.IsPrime(var12) &&
                                                        EulerMath.IsPrime(var13) && EulerMath.IsPrime(var14) &&
                                                        EulerMath.IsPrime(var15) && EulerMath.IsPrime(var16) &&
                                                        EulerMath.IsPrime(var17) && EulerMath.IsPrime(var18) &&
                                                        EulerMath.IsPrime(var19) && EulerMath.IsPrime(var20))
                                                    {
                                                        Console.WriteLine("{0},{1},{2},{3},{4}", i, j, k, m, f);
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
