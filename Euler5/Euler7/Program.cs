﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Processing ...");
            var count = 0;
            for (int i = 2;; i++)
            {
                if (EulerMath.IsPrime(i))
                    count++;
                if (count == 10001) 
                {
                    Console.WriteLine(i);
                    return;
                }
            }
        }
    }
}
