﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace Euler30
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Queue<int> res = new Queue<int>();
            for (int i = 244; i < 354294; i++)
            {
                var str = i.ToString();
                var ch = str.ToCharArray();
                int sumOfPowers=0;
                
                foreach (var item in str)
                {
                    sumOfPowers += (int)Math.Pow(Convert.ToDouble(item.ToString()), 5.0);
                }
                if (sumOfPowers == i)
                    res.Enqueue(i);
            }
            int finalRes = 0;
            foreach (var item in res)
            {
                finalRes += item;
            }
            sw.Stop();
            Console.WriteLine(finalRes);
            Console.WriteLine(sw.Elapsed);
        }
    }
}
