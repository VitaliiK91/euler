﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler56
{
    class Program
    {
        static void Main(string[] args)
        {
            int biggest = 0;
            for (BigInteger i = 0; i < 100; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    var number = BigInteger.Pow(i, j);
                    var Number = number.IntToArray();
                    int res = 0;
                    foreach (var item in Number)
                    {
                        res += item;
                    }
                    if (res > biggest)
                        biggest = res;
                }
            }
            Console.WriteLine(biggest);
        }
    }
}
