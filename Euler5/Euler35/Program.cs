﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler35
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int count = 0;
            //Console.WriteLine(EulerMath.NextRotation(123, 123));

            for (int i = 10; i < 1000000; i++)
            {
                if (EulerMath.IsPrime(i))
                {
                    bool ok = true;
                    int next = i;
                    bool more = true;
                    //Console.WriteLine(i);
                    while (more)
                    {
                        
                        next = EulerMath.NextRotation(next, i);
                        if (next == 0)
                            more = false;
                        if (!EulerMath.IsPrime(next) && next != 0)
                        {
                            ok = false;
                            more = false;
                        }
                       
                    }
                    if (ok)
                    {
                        count++;
                        Console.WriteLine(i);
                    }
                }
            }
            Console.WriteLine(count);
        }
    }
}
