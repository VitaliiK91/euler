﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler50
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger[] res = new BigInteger[1000000];
            BigInteger r = 0;
            int count = 0;
            int[] Count = new int[1000000];
            for (int i = 7; i < 1000000; i++)
            {
                if (EulerMath.IsPrime(i))
                {
                    res[i] = r + i;
                    r += i;
                    count++;
                    Count[i]=count;
                }

            }
            for (int i = 0; i < res.Length; i++)
            {
                if(EulerMath.IsPrime(res[i]) && res[i]<1000000)
                {
                    Console.WriteLine("{0} , {1}", res[i], Count[i]);
                }
                if (res[i] > 1000000)
                    return;
            }
        }
    }
}
