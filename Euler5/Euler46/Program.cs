﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace Euler46
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 2;; i++)
            {
                if (!EulerMath.IsPrime(i) && i % 2 != 0)
                {
                    if (!EulerMath.IsGoldbach(i))
                    {
                        Console.WriteLine(i);
                        return;
                    }
                }
            }
        }
    }
}
