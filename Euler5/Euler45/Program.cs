﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using MyMath;

namespace Euler45
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 145;
            BigInteger res=0;
            while (true)
            {
                i++;
                res = i * (2 * i - 1);
                if (EulerMath.IsPentagonal(res))
                {
                    Console.WriteLine(res);
                    return;
                }

            }
            
        }
    }
}
