﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyMath;
namespace Euler11
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            while (true)
            {
                int divs = 1;
                int lim = EulerMath.TriangleNumer(i);
                for (int j = 2; j <= Math.Sqrt(lim); j++)
                {
                    if (lim % j == 0)
                    {
                        divs += 2;
                    }
                    else if (j == Math.Sqrt(lim))
                    {
                        divs++;
                    }
                }
                if (divs >= 500)
                {
                    Console.WriteLine(lim);
                    return;
                }
                i++;
            }
        }
    }
}
