﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;
using System.Numerics;
using System.Diagnostics;

namespace Euler57
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BigInteger num = 17;
            BigInteger denum = 12;
            BigInteger prevN = 7;
            BigInteger prevD = 5;
            int count = 0;
            for (int i = 2; i <= 1000; i++)
            {
                var x = num;
                var y = denum;
                denum = 2 * denum + prevD;
                num = 2 * num + prevN;
                prevN = x;
                prevD = y;
                if (num.ToString().Count() > denum.ToString().Count())
                    count++;
            }
            sw.Stop();
            Console.WriteLine(count);
            Console.WriteLine(sw.ElapsedMilliseconds);
        }
    }
}
