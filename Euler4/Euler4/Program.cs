﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler4
{
    public class Program
    {
        static public bool IsPalindromic(int value)
        {
            int reverse = 0;
            int original = value;
            while (value > 0)
            {
                reverse = reverse * 10 + value % 10;
                value /= 10;
            }
            if (reverse == original)
                return true;
            else
                return false;
        }

        static void Main(string[] args)
        {
            int answer = 0;

            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    var res = i * j;
                    if (IsPalindromic(res) && res>answer)
                        answer = res;
                    
                }
            }

            Console.WriteLine("The largest palindrome made from the product of two 3-digit numbers is : {0}", answer);
        }
    }
}
