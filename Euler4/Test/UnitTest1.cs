﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Euler4;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void IsPalindromicTest()
        {
            var testArray = new int[] { 99, 909, 292, 3003, 5665, 8008008, 9 };
            foreach (var item in testArray)
            {
                Assert.IsTrue(Program.IsPalindromic(item));
            }
        }
        [TestMethod]
        public void IsPalindromicOppositeTest()
        {
            var testArray = new int[] { 56, 98, 225, 113, 56, 923582354, 456103 };
            foreach (var item in testArray)
            {
                Assert.IsFalse(Program.IsPalindromic(item));
            }
        }
    }
}
